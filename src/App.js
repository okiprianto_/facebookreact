import React, { Component } from "react";
import { Header, Body, Footer } from "./component/template";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "login",
      loginStatus: false,
    };
  }

  changedStatusLogin = () => {
    this.setState({
      loginStatus: !this.state.loginStatus,
    });
  };

  render() {
    console.log(this.state.page);
    return (
      <div>
        <Header
          statusLogin={this.changedStatusLogin}
          goTo={this.state.loginStatus}
        />
        <Body goTo={this.state.loginStatus} />
        <Footer />
      </div>
    );
  }
}

export default App;
