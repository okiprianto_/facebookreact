import React, { Component } from "react";
import "./index.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="content">
        <div className="peta">
          <p>
            Facebook helps you connect and share with the people in your life.
          </p>
          <img src="connecting.png" alt="default" />
        </div>
        <div className="form-register">
          <h3>Create an Account</h3>
          <p style={{ fontSize: "15px" }}>it's free and always will be</p>
          <p>General Information</p>
          <form>
            <div className="inline-form">
              <input type="text" className="name" placeholder="first name" />
              <input type="text" className="name" placeholder="last name" />
            </div>
            <div className="inline-form">
              <input type="email" placeholder="your email" />
              <input type="password" placeholder="new password" />
            </div>
            <p>Date of birth - Gendre</p>
            <div className="inline-form">
              <select>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
              <select>
                <option>January</option>
                <option>February</option>
                <option>Maret</option>
                <option>April</option>
              </select>
              <select>
                <option>2020</option>
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
              </select>
            </div>
            <div className="inline-form">
              <input type="radio" name="gender" value="male" />
              <label>Male</label>
              <input type="radio" name="gender" value="female" />
              <label>Female</label>
            </div>
            <p>Term and Condition</p>
            <div className="inline-form">
              <input type="checkbox" />
              <label>I Agree Trems and Condition</label>
            </div>
            <p style={{ padding: "20px 0px" }}>
              We will send you a receive sms to confirm your account make sure
              the cellphone number or email you entered is correct
            </p>
            <button
              className="btn-register"
              style={{
                backgroundColor: "#4d55bc",
                padding: "10px 10px",
                color: "white",
              }}
            >
              Register Now
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
