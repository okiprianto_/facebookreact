import React, { Component } from "react";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div
        style={{
          padding: "10px 20px",
          marginRight: "5px",
          border: "1px solid gray",
        }}
      >
        {this.props.children}
      </div>
    );
  }
}

export default Menu;
