import Header from "./header";
import Navigation from "./navigation";
import Body from "./body";
import Footer from "./footer";

export { Header, Navigation, Body, Footer };
