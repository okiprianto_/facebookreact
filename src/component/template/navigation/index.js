import React, { Component } from "react";
import Menu from "../menu";

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Menu onClickMenu={this.props}>menu 1</Menu>
        <Menu>menu 2</Menu>
        <Menu>menu 3</Menu>
      </div>
    );
  }
}

export default Navigation;
