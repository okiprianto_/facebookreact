import React, { Component } from "react";
import { Home, Login } from "../../../pages";

class Body extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { goTo } = this.props;
    console.log(goTo);
    if (goTo) {
      return <Home />;
    } else {
      return <Login />;
    }
  }
}

export default Body;
