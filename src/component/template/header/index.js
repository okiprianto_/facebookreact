import React, { Component } from "react";
import "./index.css";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }

  changeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  logginCheck = () => {
    if (this.state.username === "admin" && this.state.password === "admin") {
      console.log("berhasil login");
      this.setState({
        username: "",
        password: "",
      });
      return this.props.statusLogin();
    }
    alert("username & password salah");
  };

  render() {
    return (
      <div className="main-header">
        <div className="logo">
          <h1>facebook</h1>
        </div>
        {this.props.goTo ? (
          <div className="login-form">
            <button
              onClick={() => this.props.statusLogin()}
              className="btn-login"
            >
              Log Out
            </button>
          </div>
        ) : (
          <div className="login-form">
            <div className="form-input">
              <label>Email or Phone</label>
              <input type="text" name="username" onChange={this.changeInput} />
            </div>
            <div className="form-input">
              <label>Password</label>
              <input
                type="password"
                name="password"
                onChange={this.changeInput}
              />
            </div>
            <button onClick={this.logginCheck} className="btn-login">
              Log In
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default Header;
